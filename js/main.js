'use strict';

document.querySelector("#firstname").addEventListener("input", function() {
    document.querySelector("#showFirstname").innerHTML = this.value;
});

document.querySelector("#lastname").addEventListener("input", function(){
    document.querySelector("#showLastname").innerHTML = this.value;
});

document.querySelector("#slogan").addEventListener("change", function() {    
    document.querySelector("#showSlogan").innerHTML = this.value;
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            document.querySelector('.preview')
                .attr('src', e.target.result)
                .height(100)
        };
        reader.readAsDataURL(input.files[0]);
    }
}